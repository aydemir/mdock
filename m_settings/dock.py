from gi.repository import Gtk, Gdk, GdkPixbuf
import os, subprocess

class Settings(Gtk.ScrolledWindow):
	def __init__(self,parent):
		Gtk.Window.__init__(self)
		self.parent = parent
		self.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
		self.dock_items = ["mmenu","apps","clock","workspace","battery","wifi","sound"]

		main_box = Gtk.VBox()
		self.add(main_box)

		self.name = "Mdock"
		try:
			self.d_icon_theme = Gtk.IconTheme.get_default()
			self.icon = self.d_icon_theme.load_icon("docky",32,Gtk.IconLookupFlags.FORCE_REGULAR)
		except:
			self.icon = None
		self.title = "Mdock Settings"

		c_box = Gtk.HBox()
		main_box.pack_start(c_box,False,False,5)
		self.hor_ver_label = Gtk.Label()
		c_box.pack_start(self.hor_ver_label,False,False,5)
		self.hor_ver_combo = Gtk.ComboBoxText()
		self.hor_ver_combo.connect("changed",self.hor_ver_change)
		self.hor_ver_combo.append_text("Horizontal")
		self.hor_ver_combo.append_text("Vertical")
		c_box.pack_start(self.hor_ver_combo,True,True,5)


		#Position Left Right Up Down
		self.position_label = Gtk.Label()
		c_box.pack_start(self.position_label,False,False,5)
		self.position_combo = Gtk.ComboBoxText()
		c_box.pack_start(self.position_combo,True,True,5)

		ad_1 = Gtk.Adjustment(0, 0, 300, 1, 0, 0)
		#Dock Size
		c_box = Gtk.HBox()
		main_box.pack_start(c_box,True,False,5)
		self.size_label = Gtk.Label()
		c_box.pack_start(self.size_label,False,False,5)
		self.size_entry = Gtk.SpinButton(adjustment=ad_1, climb_rate=1, digits=0)
		self.size_entry.set_wrap(True)
		c_box.pack_start(self.size_entry,True,True,5)

		ad_2 = Gtk.Adjustment(0, 0, 300, 1, 0, 0)
		#Focus Light
		self.focus_light_label = Gtk.Label()
		c_box.pack_start(self.focus_light_label,False,False,5)
		self.focus_light_entry = Gtk.SpinButton(adjustment=ad_2, climb_rate=1, digits=0)
		self.focus_light_entry.set_wrap(True)
		c_box.pack_start(self.focus_light_entry,True,True,5)

		ad_3 = Gtk.Adjustment(0, 0, 300, 1, 0, 0)
		#Padding X
		c_box = Gtk.HBox()
		main_box.pack_start(c_box,True,False,5)
		self.pad_x_label = Gtk.Label()
		c_box.pack_start(self.pad_x_label,False,False,5)
		self.pad_x_entry = Gtk.SpinButton(adjustment=ad_3, climb_rate=1, digits=0)
		self.pad_x_entry.set_wrap(True)
		c_box.pack_start(self.pad_x_entry,True,True,5)

		ad_4 = Gtk.Adjustment(0, 0, 300, 1, 0, 0)
		#Padding Y
		self.pad_y_label = Gtk.Label()
		c_box.pack_start(self.pad_y_label,False,False,5)
		self.pad_y_entry = Gtk.SpinButton(adjustment=ad_4, climb_rate=1, digits=0)
		self.pad_y_entry.set_wrap(True)
		c_box.pack_start(self.pad_y_entry,True,True,5)


		#Dock Auto Hide
		c_box = Gtk.HBox()
		main_box.pack_start(c_box,True,False,5)
		self.dock_hide_label = Gtk.Label()
		c_box.pack_start(self.dock_hide_label,False,False,5)
		self.dock_hide_cb = Gtk.CheckButton()
		c_box.pack_start(self.dock_hide_cb,True,True,5)


		#Group Wins
		self.group_wins_label = Gtk.Label()
		c_box.pack_start(self.group_wins_label,False,False,5)
		self.group_wins_cb = Gtk.CheckButton()
		c_box.pack_start(self.group_wins_cb,True,True,5)


		#Show Hidden Apps
		self.show_dd_label = Gtk.Label()
		c_box.pack_start(self.show_dd_label,False,False,5)
		self.show_dd_cb = Gtk.CheckButton()
		c_box.pack_start(self.show_dd_cb,True,True,5)


		#Show All WS
		c_box = Gtk.HBox()
		main_box.pack_start(c_box,True,False,5)
		self.show_all_ws_label = Gtk.Label()
		c_box.pack_start(self.show_all_ws_label,False,False,5)
		self.show_all_ws_cb = Gtk.CheckButton()
		c_box.pack_start(self.show_all_ws_cb,True,True,5)


		#Dock Move WS
		self.dock_move_ws_label = Gtk.Label()
		c_box.pack_start(self.dock_move_ws_label,False,False,5)
		self.dock_move_ws_cb = Gtk.CheckButton()
		c_box.pack_start(self.dock_move_ws_cb,True,True,5)


		ad_5 = Gtk.Adjustment(0, 0, 300, 1, 0, 0)
		#Clock Font Size
		self.clock_font_s_label = Gtk.Label()
		c_box.pack_start(self.clock_font_s_label,False,False,5)
		self.clock_font_s_ent = Gtk.SpinButton(adjustment=ad_5, climb_rate=1, digits=0)
		c_box.pack_start(self.clock_font_s_ent,True,True,5)


		#Color Open Light
		c_box = Gtk.HBox()
		main_box.pack_start(c_box,True,False,5)
		self.open_light_color_lb = Gtk.Label()
		c_box.pack_start(self.open_light_color_lb,False,False,5)
		self.open_light_color_b = Gtk.ColorButton()
		self.open_light_color_b.set_use_alpha(True)
		c_box.pack_start(self.open_light_color_b,True,True,5)

		self.focus_light_color_lb = Gtk.Label()
		c_box.pack_start(self.focus_light_color_lb,False,False,5)
		self.focus_light_color_b = Gtk.ColorButton()
		self.focus_light_color_b.set_use_alpha(True)
		c_box.pack_start(self.focus_light_color_b,True,True,5)


		#Clock Color
		c_box = Gtk.HBox()
		main_box.pack_start(c_box,True,False,5)
		self.clock_color_lb = Gtk.Label()
		c_box.pack_start(self.clock_color_lb,False,False,5)
		self.clock_color_b = Gtk.ColorButton()
		self.clock_color_b.set_use_alpha(True)
		c_box.pack_start(self.clock_color_b,True,True,5)

		self.hover_color_lb = Gtk.Label()
		c_box.pack_start(self.hover_color_lb,False,False,5)
		self.hover_color_b = Gtk.ColorButton()
		self.hover_color_b.set_use_alpha(True)
		c_box.pack_start(self.hover_color_b,True,True,5)



		#Dock Items
		c_box = Gtk.HBox()
		main_box.pack_start(c_box,True,True,5)
		self.dock_list_store = Gtk.ListStore(str)
		self.dock_list = Gtk.TreeView(model=self.dock_list_store)
		self.dock_list.set_activate_on_single_click(True)
		self.dock_list.connect("row-activated",self.dock_list_clicked)
		renderer = Gtk.CellRendererText()
		coloumn = Gtk.TreeViewColumn("Items",renderer, text = 0)
		self.dock_list.append_column(coloumn)

		scroll = Gtk.ScrolledWindow()
		scroll.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
		scroll.add(self.dock_list)
		c_box.pack_start(scroll,True,True,5)

		b_box = Gtk.VBox()
		c_box.pack_start(b_box,False,False,5)
		image = Gtk.Image()
		image.set_from_stock(Gtk.STOCK_GO_UP,Gtk.IconSize.BUTTON)

		self.up_list = Gtk.Button()
		self.up_list.connect("clicked",self.up_dock_list)
		self.up_list.set_image(image)
		b_box.pack_start(self.up_list,False,True,5)
		image = Gtk.Image()
		image.set_from_stock(Gtk.STOCK_GO_DOWN,Gtk.IconSize.BUTTON)
		self.down_list = Gtk.Button()
		self.down_list.connect("clicked",self.down_dock_list)
		self.down_list.set_image(image)
		b_box.pack_start(self.down_list,False,False,5)
		image = Gtk.Image()
		image.set_from_stock(Gtk.STOCK_ADD,Gtk.IconSize.BUTTON)
		self.add_list = Gtk.Button()
		self.add_list.connect("clicked",self.add_dock_list)
		self.add_list.set_image(image)
		b_box.pack_start(self.add_list,False,False,5)
		image = Gtk.Image()
		image.set_from_stock(Gtk.STOCK_REMOVE,Gtk.IconSize.BUTTON)
		self.remove_list = Gtk.Button()
		self.remove_list.connect("clicked",self.remove_dock_list)
		self.remove_list.set_image(image)
		b_box.pack_start(self.remove_list,False,False,5)


		#Dock Items
		self.pins_list_store = Gtk.ListStore(GdkPixbuf.Pixbuf,str)
		self.pins_list = Gtk.TreeView(model=self.pins_list_store)
		self.pins_list.set_activate_on_single_click(True)
		self.pins_list.connect("row-activated",self.pin_list_clicked)
		renderer = Gtk.CellRendererPixbuf()
		coloumn = Gtk.TreeViewColumn("Icon",renderer, gicon = 0)
		self.pins_list.append_column(coloumn)
		renderer = Gtk.CellRendererText()
		coloumn = Gtk.TreeViewColumn("Pınned Apps",renderer, text = 1)
		self.pins_list.append_column(coloumn)

		scroll = Gtk.ScrolledWindow()
		scroll.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
		scroll.add(self.pins_list)
		c_box.pack_start(scroll,True,True,5)

		b_box = Gtk.VBox()
		c_box.pack_start(b_box,False,False,5)
		image = Gtk.Image()
		image.set_from_stock(Gtk.STOCK_GO_UP,Gtk.IconSize.BUTTON)
		self.up_pin = Gtk.Button()
		self.up_pin.connect("clicked",self.up_pin_list)
		self.up_pin.set_image(image)
		b_box.pack_start(self.up_pin,False,True,5)
		image = Gtk.Image()
		image.set_from_stock(Gtk.STOCK_GO_DOWN,Gtk.IconSize.BUTTON)
		self.down_pin = Gtk.Button()
		self.down_pin.connect("clicked",self.down_pin_list)
		self.down_pin.set_image(image)
		b_box.pack_start(self.down_pin,False,False,5)
		image = Gtk.Image()
		image.set_from_stock(Gtk.STOCK_ADD,Gtk.IconSize.BUTTON)
		image = Gtk.Image()
		image.set_from_stock(Gtk.STOCK_REMOVE,Gtk.IconSize.BUTTON)
		self.remove_pin = Gtk.Button()
		self.remove_pin.connect("clicked",self.remove_pin_list)
		self.remove_pin.set_image(image)
		b_box.pack_start(self.remove_pin,False,False,5)



		c_box = Gtk.HBox()
		main_box.pack_start(c_box,True,True,5)
		self.apply_button = Gtk.Button()
		self.apply_button.connect("clicked",self.write_config_file)
		c_box.pack_start(self.apply_button,True,True,5)


		self.labels_set_texts()
		self.read_config_file()


	def up_pin_list(self,widget):
		item = self.pin_list_clicked()
		if item:
			ind = self.pinned_apps.index(item)
			if ind != 0:
				n_ind = ind - 1
				self.pinned_apps.insert(n_ind,item)
				self.pinned_apps.pop(ind+1)
			self.pinned_store_add()


	def down_pin_list(self,widget):
		item = self.pin_list_clicked()
		if item:
			ind = self.pinned_apps.index(item)
			if ind != len(self.pinned_apps) - 1:
				n_ind = ind + 2
				self.pinned_apps.insert(n_ind,item)
				self.pinned_apps.pop(ind)
			self.pinned_store_add()


	def remove_pin_list(self,widget):
		item = self.pin_list_clicked()
		if item:
			self.pinned_apps.remove(item)
			self.pinned_store_add()




	def up_dock_list(self,widget):
		item = self.dock_list_clicked()
		if item:
			ind = self.draw_o.index(item)
			if ind != 0:
				n_ind = ind - 1
				self.draw_o.insert(n_ind,item)
				self.draw_o.pop(ind+1)
				self.dock_list_store_add()


	def down_dock_list(self,widget):
		item = self.dock_list_clicked()
		if item:
			ind = self.draw_o.index(item)
			if ind != len(self.draw_o) - 1:
				n_ind = ind + 2
				self.draw_o.insert(n_ind,item)
				self.draw_o.pop(ind)
				self.dock_list_store_add()


	def remove_dock_list(self,widget):
		item = self.dock_list_clicked()
		if item:
			self.draw_o.remove(item)
			self.dock_list_store_add()


	def add_dock_list(self,widget):
		popup_menu = Gtk.Menu()
		add = 0
		for i in self.dock_items:
			if i not in self.draw_o:
				add += 1
				item = Gtk.MenuItem(i)
				item.connect("activate",self.add_dock,i)
				popup_menu.append(item)
		if add:
			popup_menu.show_all()
			popup_menu.popup_at_pointer()

	def add_dock(self,widget,item):
		self.draw_o.append(item)
		self.dock_list_store_add()


	def pin_list_clicked(self,widget=None,path=None,coloumn=None):
		try:
			selection = self.pins_list.get_selection()
			tree_model, tree_iter = selection.get_selected()
			s = tree_model[tree_iter][1]
			return s
		except:
			return False



	def dock_list_clicked(self,widget=None,path=None,coloumn=None):
		try:
			selection = self.dock_list.get_selection()
			tree_model, tree_iter = selection.get_selected()
			s = tree_model[tree_iter][0]
			return s
		except:
			return False


	def labels_set_texts(self):
		self.hor_ver_label.set_text("Dock Settlement")
		self.position_label.set_text("Dock Position")
		self.size_label.set_text("Dock Size")
		self.pad_x_label.set_text("Horizontal Space")
		self.pad_y_label.set_text("Vertical Space")
		self.focus_light_label.set_text("Focus Light Size")
		self.dock_hide_label.set_text("Dock Auto Hide")
		self.group_wins_label.set_text("Group Applications")
		self.show_dd_label.set_text("Show Hidden Apps")
		self.show_all_ws_label.set_text("Show All WorkSpace")
		self.dock_move_ws_label.set_text("Dock Move WorkSpace")
		self.clock_font_s_label.set_text("Clock Font Size")
		self.open_light_color_lb.set_text("Open Light Color")
		self.focus_light_color_lb.set_text("Focus Light Color")
		self.clock_color_lb.set_text("Clock Font Color")
		self.hover_color_lb.set_text("Hover Color")
		self.apply_button.set_label("Apply")


	def hor_ver_change(self, widget):
		text = widget.get_active_text()
		self.position_combo.remove_all()
		if text == "Horizontal":
			self.position_combo.append_text("Up-Left")
			self.position_combo.append_text("Up-Center")
			self.position_combo.append_text("Up-Right")
			self.position_combo.append_text("Down-Left")
			self.position_combo.append_text("Down-Center")
			self.position_combo.append_text("Down-Right")
		elif text == "Vertical":
			self.position_combo.append_text("Left-Up")
			self.position_combo.append_text("Left-Center")
			self.position_combo.append_text("Left-Down")
			self.position_combo.append_text("Right-Up")
			self.position_combo.append_text("Right-Center")
			self.position_combo.append_text("Right-Down")
		self.position_combo.set_active(0)


	def pinned_store_add(self):
		self.pins_list_store.clear()
		for a in self.pinned_apps:
			icon_path = "~/.config/mdock/pins/{}".format(a)
			icon_path = os.path.expanduser(icon_path)
			icon = GdkPixbuf.Pixbuf.new_from_file(icon_path)
			self.pins_list_store.append([icon,a])


	def dock_list_store_add(self):
		self.dock_list_store.clear()
		for a in self.draw_o:
			self.dock_list_store.append([a])


	def read_config_file(self):
		dir_ = "~/.config/mdock/config"
		dir_ = os.path.expanduser(dir_)
		if os.path.exists(dir_):
			f = open(dir_,"r")
			read_ = f.read()
			f.close()
			read_ = read_.split("\n")
			for read in read_:
				read = read.split(":")
				if read[0] == "hor_ver":
					self.hor_ver = int(read[1])
					self.hor_ver_combo.set_active(self.hor_ver)
				elif read[0] == "dock_auto_hide":
					self.dock_hide_cb.set_active(int(read[1]))
				elif read[0] == "position_x":
					pos_x = int(read[1])
				elif read[0] == "position_y":
					pos_y = int(read[1])
				elif read[0] == "size_x":
					self.size_entry.set_value(int(read[1]))
				elif read[0] == "size_y":
					self.size_entry.set_value(int(read[1]))
				elif read[0] == "padding_x":
					self.pad_x_entry.set_value(int(read[1]))
				elif read[0] == "padding_y":
					self.pad_y_entry.set_value(int(read[1]))
				elif read[0] == "size_focus_light":
					self.focus_light_entry.set_value(int(read[1]))
				elif read[0] == "group_wins":
					self.group_wins_cb.set_active(int(read[1]))
				elif read[0] == "show_desktop_dock":
					self.show_dd_cb.set_active(int(read[1]))
				elif read[0] == "show_all_workspace":
					self.show_all_ws_cb.set_active(int(read[1]))
				elif read[0] == "dock_move_workspace":
					self.dock_move_ws_cb.set_active(int(read[1]))
				elif read[0] == "clock_font_size":
					self.clock_font_s_ent.set_value(int(read[1]))
				elif read[0] == "draw_o":
					if read[1] != "":
						a = read[1].replace(" ","")
						self.draw_o = a.split(",")
						self.dock_list_store_add()
				elif read[0] == "pinned_apps":
					if read[1] != "":
						a = read[1].replace(" ","")
						self.pinned_apps = a.split(",")
						self.pinned_store_add()
				elif read[0] == "light_color_focus":
					a = read[1].split(",")
					light_color_focus = Gdk.RGBA(float(a[0]),float(a[1]),float(a[2]),float(a[3]))
					self.focus_light_color_b.set_rgba(light_color_focus)
					#self.open_light_color_b.set_alpha(float(a[3])*65535)
				elif read[0] == "light_color_open":
					a = read[1].split(",")
					light_color_open = Gdk.RGBA(float(a[0]),float(a[1]),float(a[2]),float(a[3]))
					self.open_light_color_b.set_rgba(light_color_open)
				elif read[0] == "clock_color":
					a = read[1].split(",")
					clock_color = Gdk.RGBA(float(a[0]),float(a[1]),float(a[2]),float(a[3]))
					self.clock_color_b.set_rgba(clock_color)
				elif read[0] == "hover_color":
					a = read[1].split(",")
					hover_color = Gdk.RGBA(float(a[0]),float(a[1]),float(a[2]),float(a[3]))
					self.hover_color_b.set_rgba(hover_color)
			if self.hor_ver:
				if pos_x == 0:
					self.position_combo.set_active(pos_y)
				else:
					self.position_combo.set_active(pos_y+3)
			else:
				if pos_y == 0:
					self.position_combo.set_active(pos_x)
				else:
					self.position_combo.set_active(pos_x+3)


	def get_button_color(self,button):
		color = button.get_color().to_floats()
		color = list(color)
		alpha = button.get_alpha()
		alpha = alpha / 65535
		color.append(alpha)
		return color


	def write_config_file(self, widget):
		a = False
		if self.size_entry.get_text() == "":
			a = "Dock Size İs Empty"
		elif self.pad_x_entry.get_text() == "":
			a = "Horizontal Space İs Empty"
		elif self.pad_y_entry.get_text() == "":
			a = "Vertical Space İs Empty"
		elif self.focus_light_entry.get_text() == "":
			a = "Focus Light Size İs Empty"
		elif self.clock_font_s_ent.get_text() == "":
			a = "Clock Font Size  İs Empty"

		if a:
			dialog = Gtk.MessageDialog(self.parent, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.OK, "Dont Apply")
			dialog.format_secondary_text(a)
			dialog.run()
			dialog.destroy()
			return False

		dir_ = "~/.config/mdock/config"
		dir_ = os.path.expanduser(dir_)

		config = "dock_auto_hide:{}\n".format(str(int(self.dock_hide_cb.get_active())))
		config += "hor_ver:{}\n".format(str(self.hor_ver_combo.get_active()))
		pos = self.position_combo.get_active()
		if self.hor_ver_combo.get_active():
			if pos // 3 == 0:
				pos_x = 0
				pos_y = pos
			else:
				pos_x = 2
				pos_y = pos - 3
		else:
			if pos // 3 == 0:
				pos_y = 0
				pos_x = pos
			else:
				pos_y = 2
				pos_x = pos - 3
		config += "position_x:{}\n".format(str(pos_x))
		config += "position_y:{}\n".format(str(pos_y))
		config += "size_x:{}\n".format(self.size_entry.get_text())
		config += "size_y:{}\n".format(self.size_entry.get_text())
		config += "padding_x:{}\n".format(self.pad_x_entry.get_text())
		config += "padding_y:{}\n".format(self.pad_y_entry.get_text())
		config += "size_focus_light:{}\n".format(self.focus_light_entry.get_text())
		config += "group_wins:{}\n".format(int(self.group_wins_cb.get_active()))
		config += "show_desktop_dock:{}\n".format(int(self.show_dd_cb.get_active()))
		config += "show_all_workspace:{}\n".format(int(self.show_all_ws_cb.get_active()))
		config += "dock_move_workspace:{}\n".format(int(self.dock_move_ws_cb.get_active()))
		config += "clock_font_size:{}\n".format(self.clock_font_s_ent.get_text())
		config += "draw_o:{}\n".format(str(self.draw_o)[1:-1].replace("'",""))
		config += "pinned_apps:{}\n".format(str(self.pinned_apps)[1:-1].replace("'",""))
		config += "light_color_focus:{}\n".format(str(self.get_button_color(self.focus_light_color_b))[1:-1])
		config += "light_color_open:{}\n".format(str(self.get_button_color(self.open_light_color_b))[1:-1])
		config += "clock_color:{}\n".format(str(self.get_button_color(self.clock_color_b))[1:-1])
		config += "hover_color:{}".format(str(self.get_button_color(self.hover_color_b))[1:-1])
		f = open(dir_,"w")
		f.write(config)
		f.close()
		self.remove_mdock()


	def remove_mdock(self):
		subprocess.Popen(["killall","mdock"])
		subprocess.Popen(["mdock"])
		
