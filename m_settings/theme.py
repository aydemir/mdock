from gi.repository import Gtk, Gdk, GdkPixbuf
import os, subprocess

class Settings(Gtk.ScrolledWindow):
	def __init__(self,parent):
		Gtk.Window.__init__(self)
		self.parent = parent
		self.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)

		main_box = Gtk.VBox()
		self.add(main_box)

		self.name = "Theme"
		try:
			self.d_icon_theme = Gtk.IconTheme.get_default()
			self.icon = self.d_icon_theme.load_icon("xfce-theme-manager",32,Gtk.IconLookupFlags.FORCE_REGULAR)
		except:
			self.icon = None
		self.title = "Thene Settings"


		self.icon_themes = self.get_theme_names(["/usr/share/icons","~/.local/share/icons"])
		self.icon_themes.sort()
		self.gtk_themes = self.get_theme_names(["/usr/share/themes","~/.local/share/themes"])
		self.gtk_themes.sort()
		self.cursor_themes = self.get_cursors(["/usr/share/icons","~/.local/share/icons"],"cursors")
		self.gtk_toolbar_styles = ["GTK_TOOLBAR_ICONS","GTK_TOOLBAR_TEXT","GTK_TOOLBAR_BOTH","GTK_TOOLBAR_BOTH_HORIZ"]
		self.gtk_toolbar_icon_size = ["GTK_ICON_SIZE_INVALID","GTK_ICON_SIZE_MENU","GTK_ICON_SIZE_SMALL_TOOLBAR",
									"GTK_ICON_SIZE_LARGE_TOOLBAR","GTK_ICON_SIZE_BUTTON","GTK_ICON_SIZE_DND",
									"GTK_ICON_SIZE_DIALOG"]
		self.hints = ["hintnone","hintslight","hintmedium","hintfull"]


		c_box = Gtk.HBox()
		main_box.pack_start(c_box,True,False,5)
		self.icon_theme_label = Gtk.Label()
		c_box.pack_start(self.icon_theme_label,False,False,5)
		self.icon_theme_combo = Gtk.ComboBoxText()
		c_box.pack_start(self.icon_theme_combo,True,True,5)
		self.combo_append(self.icon_theme_combo,self.icon_themes)

		self.gtk_theme_label = Gtk.Label()
		c_box.pack_start(self.gtk_theme_label,False,False,5)
		self.gtk_theme_combo = Gtk.ComboBoxText()
		self.gtk_theme_combo.connect("changed",self.gtk_theme_change)
		c_box.pack_start(self.gtk_theme_combo,True,True,5)
		self.combo_append(self.gtk_theme_combo,self.gtk_themes)


		c_box = Gtk.HBox()
		main_box.pack_start(c_box,True,False,5)
		self.hints_label = Gtk.Label()
		c_box.pack_start(self.hints_label,False,False,5)
		self.hints_combo = Gtk.ComboBoxText()
		c_box.pack_start(self.hints_combo,True,True,5)
		self.combo_append(self.hints_combo,self.hints)

		self.cursor_theme_label = Gtk.Label()
		c_box.pack_start(self.cursor_theme_label,False,False,5)
		self.cursor_theme_combo = Gtk.ComboBoxText()
		c_box.pack_start(self.cursor_theme_combo,True,True,5)
		self.combo_append(self.cursor_theme_combo,self.cursor_themes)


		c_box = Gtk.HBox()
		main_box.pack_start(c_box,True,False,5)
		self.tool_bar_style_lb = Gtk.Label()
		c_box.pack_start(self.tool_bar_style_lb,False,False,5)
		self.tool_bar_style_cb = Gtk.ComboBoxText()
		c_box.pack_start(self.tool_bar_style_cb,True,True,5)
		self.combo_append(self.tool_bar_style_cb,self.gtk_toolbar_styles)


		c_box = Gtk.HBox()
		main_box.pack_start(c_box,True,False,5)
		self.font_label = Gtk.Label()
		c_box.pack_start(self.font_label,False,False,5)
		self.font_button = Gtk.FontButton()
		c_box.pack_start(self.font_button,True,True,5)


		c_box = Gtk.HBox()
		main_box.pack_start(c_box,True,False,5)
		self.tool_bar_icon_s_lb = Gtk.Label()
		c_box.pack_start(self.tool_bar_icon_s_lb,False,False,5)
		self.tool_bar_icon_s_cb = Gtk.ComboBoxText()
		c_box.pack_start(self.tool_bar_icon_s_cb,True,True,5)
		self.combo_append(self.tool_bar_icon_s_cb,self.gtk_toolbar_icon_size)


		ad = Gtk.Adjustment(0, 0, 300, 1, 0, 0)
		#Focus Light
		self.cursor_theme_size_c = Gtk.Label()
		c_box.pack_start(self.cursor_theme_size_c,False,False,5)
		self.cursor_theme_size_e = Gtk.SpinButton(adjustment=ad, climb_rate=1, digits=0)
		self.cursor_theme_size_e.set_wrap(True)
		c_box.pack_start(self.cursor_theme_size_e,True,True,5)


		c_box = Gtk.HBox()
		main_box.pack_start(c_box,True,False,5)
		self.show_b_images_lb = Gtk.Label()
		c_box.pack_start(self.show_b_images_lb,False,False,5)
		self.show_b_images_cb = Gtk.CheckButton()
		c_box.pack_start(self.show_b_images_cb,True,True,5)

		self.show_m_images_lb = Gtk.Label()
		c_box.pack_start(self.show_m_images_lb,False,False,5)
		self.show_m_images_cb = Gtk.CheckButton()
		c_box.pack_start(self.show_m_images_cb,True,True,5)


		self.enable_event_s_lb = Gtk.Label()
		c_box.pack_start(self.enable_event_s_lb,False,False,5)
		self.enable_event_s_cb = Gtk.CheckButton()
		c_box.pack_start(self.enable_event_s_cb,True,True,5)


		c_box = Gtk.HBox()
		main_box.pack_start(c_box,True,False,5)
		self.e_input_feedback_s_lb = Gtk.Label()
		c_box.pack_start(self.e_input_feedback_s_lb,False,False,5)
		self.e_input_feedback_s_cb = Gtk.CheckButton()
		c_box.pack_start(self.e_input_feedback_s_cb,True,True,5)


		self.xft_antialias_lb = Gtk.Label()
		c_box.pack_start(self.xft_antialias_lb,False,False,5)
		self.xft_antialias_cb = Gtk.CheckButton()
		c_box.pack_start(self.xft_antialias_cb,True,True,5)


		self.hinting_lb = Gtk.Label()
		c_box.pack_start(self.hinting_lb,False,False,5)
		self.hinting_cb = Gtk.CheckButton()
		c_box.pack_start(self.hinting_cb,True,True,5)

		c_box = Gtk.HBox()
		main_box.pack_start(c_box,True,False,5)
		self.apply_button = Gtk.Button()
		self.apply_button.connect("clicked",self.write_config_file)
		c_box.pack_start(self.apply_button,True,True,5)


		self.read_settings_file()
		self.labels_set_texts()


	def gtk_theme_change(self,widget):
		theme = widget.get_active_text()
		css = Gtk.CssProvider()
		css = css.get_named(theme,None)

		Gtk.StyleContext.add_provider_for_screen(
			Gdk.Screen.get_default(),
			css,
			Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
	    )




	def write_cursor(self):
		if self.cursor_theme_combo.get_active_text() == self.active_cursor_theme:
			return False
		else:
			self.active_cursor_theme = self.cursor_theme_combo.get_active_text()
		dir_ = os.path.expanduser("~/.Xdefaults")
		if os.path.exists(dir_):
			f = open(dir_,"r")
			reads = f.read()
			f.close()
			reads = reads.split("\n")
			writes = ""
			for read in reads:
				type_ = read.split(":")
				if type_[0] == "Xcursor.theme":
					writes += "Xcursor.theme:{}\n".format(self.cursor_theme_combo.get_active_text())
				else:
					writes += read+"\n"
			if "Xcursor.theme:" not in writes:
				writes += "Xcursor.theme:{}\n".format(self.cursor_theme_combo.get_active_text())
		else:
			writes = "Xcursor.theme:{}\n".format(self.cursor_theme_combo.get_active_text())
		f = open(dir_,"w")
		reads = f.write(writes)
		f.close()


		dir_ = os.path.expanduser("~/.icons/default/")
		if not os.path.exists(dir_):
			os.makedirs(dir_)
		dir_ = os.path.join(dir_,"index.theme")
		if os.path.exists(dir_):
			f = open(dir_,"r")
			reads = f.read()
			f.close()
			reads = reads.split("\n")
			writes = ""
			for read in reads:
				type_ = read.split("=")
				if type_[0] == "Inherits":
					writes += "Inherits={}\n".format(self.cursor_theme_combo.get_active_text())
				else:
					writes += read+"\n"
			if "Inherits" not in writes:
				writes += "Inherits={}\n".format(self.cursor_theme_combo.get_active_text())
		else:
			writes = "Inherits={}\n".format(self.cursor_theme_combo.get_active_text())
		f = open(dir_,"w")
		reads = f.write(writes)
		f.close()

		subprocess.Popen(["openbox","--restart"])



	def write_config_file(self,widget):
		dir_1 =  os.path.expanduser("~/.config/gtk-3.0/settings.ini")
		dir_2 =  os.path.expanduser("~/.gtkrc-2.0")
		write_1 = "[Settings]\n"
		write_2 = 'include "{}.mine"\n'.format(dir_2)
		gtk_theme_name = self.gtk_theme_combo.get_active_text()
		write_1 += 'gtk-theme-name={}\n'.format(gtk_theme_name)
		write_2 += 'gtk-theme-name="{}"\n'.format(gtk_theme_name)

		gtk_icon_theme = self.icon_theme_combo.get_active_text()
		write_1 += 'gtk-icon-theme-name={}\n'.format(gtk_icon_theme)
		write_2 += 'gtk-icon-theme-name="{}"\n'.format(gtk_icon_theme)

		gtk_font_name = self.font_button.get_font()
		write_1 += 'gtk-font-name={}\n'.format(gtk_font_name)
		write_2 += 'gtk-font-name="{}"\n'.format(gtk_font_name)

		gtk_cursor_theme_name = self.cursor_theme_combo.get_active_text()
		write_1 += 'gtk-cursor-theme-name={}\n'.format(gtk_cursor_theme_name)
		write_2 += 'gtk-cursor-theme-name={}\n'.format(gtk_cursor_theme_name)

		gtk_cursor_theme_size = self.cursor_theme_size_e.get_text()
		write_1 += 'gtk-cursor-theme-size={}\n'.format(gtk_cursor_theme_size)
		write_2 += 'gtk-cursor-theme-size={}\n'.format(gtk_cursor_theme_size)

		gtk_toolbar_style = self.tool_bar_style_cb.get_active_text()
		write_1 += 'gtk-toolbar-style={}\n'.format(gtk_toolbar_style)
		write_2 += 'gtk-toolbar-style={}\n'.format(gtk_toolbar_style)

		gtk_toolbar_icon_size = self.tool_bar_icon_s_cb.get_active_text()
		write_1 += 'gtk-toolbar-icon-size={}\n'.format(gtk_toolbar_icon_size)
		write_2 += 'gtk-toolbar-icon-size={}\n'.format(gtk_toolbar_icon_size)

		gtk_button_images = str(int(self.show_b_images_cb.get_active()))
		write_1 += 'gtk-button-images={}\n'.format(gtk_button_images)
		write_2 += 'gtk-button-images={}\n'.format(gtk_button_images)

		gtk_menu_images = str(int(self.show_m_images_cb.get_active()))
		write_1 += 'gtk-menu-images={}\n'.format(gtk_menu_images)
		write_2 += 'gtk-menu-images={}\n'.format(gtk_menu_images)

		gtk_enable_event_sounds = str(int(self.enable_event_s_cb.get_active()))
		write_1 += 'gtk-enable-event-sounds={}\n'.format(gtk_enable_event_sounds)
		write_2 += 'gtk-enable-event-sounds={}\n'.format(gtk_enable_event_sounds)

		gtk_enable_input_feedback_sounds = str(int(self.e_input_feedback_s_cb.get_active()))
		write_1 += 'gtk-enable-input-feedback-sounds={}\n'.format(gtk_enable_input_feedback_sounds)
		write_2 += 'gtk-enable-input-feedback-sounds={}\n'.format(gtk_enable_input_feedback_sounds)

		gtk_xft_antialias = str(int(self.xft_antialias_cb.get_active()))
		write_1 += 'gtk-xft-antialias={}\n'.format(gtk_xft_antialias)
		write_2 += 'gtk-xft-antialias={}\n'.format(gtk_xft_antialias)

		gtk_xft_hinting = str(int(self.hinting_cb.get_active()))
		write_1 += 'gtk-xft-hinting={}\n'.format(gtk_xft_hinting)
		write_2 += 'gtk-xft-hinting={}\n'.format(gtk_xft_hinting)

		gtk_xft_hintstyle = self.hints_combo.get_active_text()
		write_1 += 'gtk-xft-hintstyle={}\n'.format(gtk_xft_hintstyle)
		write_2 += 'gtk-xft-hintstyle="{}"\n'.format(gtk_xft_hintstyle)

		f = open(dir_1,"w")
		f.write(write_1)
		f.close()
		f = open(dir_2,"w")
		f.write(write_2)
		f.close()
		self.write_cursor()



	def read_settings_file(self):
		dir_1 =  os.path.expanduser("~/.config/gtk-3.0/settings.ini")
		dir_2 =  os.path.expanduser("~/.gtkrc-2.0")
		if os.path.exists(dir_1):
			dir_ = dir_1
			a = False
		elif os.path.exists(dir_2):
			dir_ = dir_2
			a = True
		else:
			self.icon_theme_combo.set_active(0)
			self.gtk_theme_combo.set_active(0)
			self.hints_combo.set_active(0)
			self.cursor_theme_combo.set_active(0)
			self.tool_bar_style_cb.set_active(0)
			self.tool_bar_icon_s_cb.set_active(0)
			self.active_cursor_theme = self.cursor_theme_combo.get_active_text()
			dialog = Gtk.MessageDialog(self.parent, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.OK, "Dont Read Eror")
			dialog.format_secondary_text("~/.gtkrc-2.0 or ~/.config/gtk-3.0/settings.ini not found.")
			dialog.run()
			dialog.destroy()
			return False
		f = open(dir_,"r")
		reads = f.read()
		f.close()
		for read in reads.split("\n"):
			b = self.split_line(read)
			if not b:
				continue
			if b[0] == "gtk-theme-name":
				if a:
					b[1] = b[1][1:-1]
				c = self.gtk_themes.index(b[1])
				self.gtk_theme_combo.set_active(c)
			elif b[0] == "gtk-icon-theme-name":
				if a:
					b[1] = b[1][1:-1]
				c = self.icon_themes.index(b[1])
				self.icon_theme_combo.set_active(c)
			elif b[0] == "gtk-font-name":
				self.font_button.set_font_name(b[1])
			elif b[0] == "gtk-cursor-theme-name":
				if a:
					b[1] = b[1][1:-1]
				c = self.cursor_themes.index(b[1])
				self.active_cursor_theme = self.cursor_themes[c]
				self.cursor_theme_combo.set_active(c)
			elif b[0] == "gtk-cursor-theme-size":
				self.cursor_theme_size_e.set_value(int(b[1]))
			elif b[0] == "gtk-toolbar-style":
				c = self.gtk_toolbar_styles.index(b[1])
				self.tool_bar_style_cb.set_active(c)
			elif b[0] == "gtk-toolbar-icon-size":
				c = self.gtk_toolbar_icon_size.index(b[1])
				self.tool_bar_icon_s_cb.set_active(c)
			elif b[0] == "gtk-xft-hintstyle":
				if a:
					b[1] = b[1][1:-1]
				c = self.hints.index(b[1])
				self.hints_combo.set_active(c)
			elif b[0] == "gtk-button-images":
				self.show_b_images_cb.set_active(int(b[1]))
			elif b[0] == "gtk-menu-images":
				self.show_m_images_cb.set_active(int(b[1]))
			elif b[0] == "gtk-enable-event-sounds":
				self.enable_event_s_cb.set_active(int(b[1]))
			elif b[0] == "gtk-enable-input-feedback-sounds":
				self.e_input_feedback_s_cb.set_active(int(b[1]))
			elif b[0] == "gtk-xft-antialias":
				self.xft_antialias_cb.set_active(int(b[1]))
			elif b[0] == "gtk-xft-hinting":
				self.hinting_cb.set_active(int(b[1]))


	def split_line(self, line):
		line = line.split("=")
		if len(line) == 2:
			a = line[0].replace(" ","")
			b = line[1]
			if b[0] == " ":
				b = b[1:]
			return (a,b)
		else:
			return False


	def labels_set_texts(self):
		self.icon_theme_label.set_text("Icon Theme")
		self.gtk_theme_label.set_text("Gtk Theme")
		self.cursor_theme_label.set_text("Cursor Theme")
		self.tool_bar_style_lb.set_text("Toolbar Style")
		self.tool_bar_icon_s_lb.set_text("Toolbar Icon")
		self.hints_label.set_text("Hints")
		self.cursor_theme_size_c.set_text("Cursor Theme Size")
		self.show_b_images_lb.set_text("Show Button Images")
		self.show_m_images_lb.set_text("Show Menu Images")
		self.enable_event_s_lb.set_text("Enable Event Sounds")
		self.e_input_feedback_s_lb.set_text("Enable Input Feedback Sounds")
		self.xft_antialias_lb.set_text("Antialias")
		self.hinting_lb.set_text("Hinting")
		self.apply_button.set_label("Apply")
		self.font_label.set_label("Font Type")


	def combo_append(self,combo,list_):
		combo.remove_all()
		for item in list_:
			combo.append_text(item)


	def get_cursors(self,dirs,type_):
		r_list = []
		for dir_ in dirs:
			dir_ = os.path.expanduser(dir_)
			if os.path.exists(dir_):
				icons = os.listdir(dir_)
				for icon in icons:
					index = os.path.join(dir_,icon,type_)
					if os.path.exists(index):
						r_list.append(icon)
		return r_list


	def get_theme_names(self,dirs):
		r_list = []
		for dir_ in dirs:
			dir_ = os.path.expanduser(dir_)
			if os.path.exists(dir_):
				icons = os.listdir(dir_)
				for icon in icons:
					index = os.path.join(dir_,icon,"index.theme")
					if os.path.exists(index):
						name = self.get_read_index_file(index)
						if name != "None":
							r_list.append(icon)
		return r_list



	def get_read_index_file(self,file_):
		f = open(file_,"r")
		reads = f.read()
		f.close()
		for coloum in reads.split():
			c = coloum.split("=")
			if c[0] == "Name":
				name = c[1]
				return name
		return "None"


