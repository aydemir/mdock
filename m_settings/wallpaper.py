from gi.repository import Gtk, Gdk, GdkPixbuf
import os, subprocess

class Settings(Gtk.ScrolledWindow):
	def __init__(self,parent):
		Gtk.Window.__init__(self)
		self.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)

		main_box = Gtk.VBox()
		self.add(main_box)

		self.name = "Wallpaper"
		try:
			self.d_icon_theme = Gtk.IconTheme.get_default()
			self.icon = self.d_icon_theme.load_icon("ristretto",32,Gtk.IconLookupFlags.FORCE_REGULAR)
		except:
			self.icon = None
		self.title = "Wallpaper Settings"


		c_box = Gtk.HBox()
		main_box.pack_start(c_box,True,True,5)
		self.images_store = Gtk.ListStore(str,GdkPixbuf.Pixbuf)
		self.images_list = Gtk.IconView(model=self.images_store)
		self.images_list.set_text_column(0)
		self.images_list.set_pixbuf_column(1)
		self.images_list.set_item_width(120)
		scroll = Gtk.ScrolledWindow()
		scroll.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
		scroll.add(self.images_list)
		c_box.pack_start(scroll,True,True,5)


		c_box = Gtk.HBox()
		main_box.pack_start(c_box,False,False,5)
		self.file_choser = Gtk.FileChooserButton()
		self.file_choser.set_action(Gtk.FileChooserAction.SELECT_FOLDER)
		self.file_choser.connect("file-set",self.select_folder)
		self.file_choser.set_current_folder("/usr/share/backgrounds/xfce")
		c_box.pack_start(self.file_choser,False,False,5)


		self.type_label = Gtk.Label()
		c_box.pack_start(self.type_label,False,False,5)

		self.type_combo = Gtk.ComboBoxText()
		types = ["scale","tile","center","max","fill"]
		for type_ in types:
			self.type_combo.append_text(type_)
		self.type_combo.set_active(0)
		c_box.pack_start(self.type_combo,False,False,5)


		self.apply_button = Gtk.Button()
		self.apply_button.connect("clicked",self.apply_button_clicked)
		c_box.pack_start(self.apply_button,True,True,5)

		self.set_labels()
		self.add_images("/usr/share/backgrounds/xfce")


	def apply_button_clicked(self,widget):
		selection = self.images_list.get_selected_items()
		if len(selection) == 0:
			return False
		else:
			selection = selection[0]
		image = self.images_store[selection][0]
		folder = self.file_choser.get_filename()
		dir_ = os.path.join(folder,image)
		type_ = self.type_combo.get_active_text()

		command = "feh --bg-{} {}".format(type_,dir_)
		subprocess.Popen(command.split())
		self.write_autostart(command)


	def write_autostart(self,command):
		a_dir = os.path.expanduser("~/.config/openbox/autostart")
		f = open(a_dir,"r")
		reads = f.read()
		f.close()
		writes = ""
		for read in reads.split("\n"):
			control = read.split()
			if len(control) != 0 and control[0] == "feh":
				writes += command+" &\n"
			else:
				if read != "":
					writes += read+"\n"
		f = open(a_dir,"w")
		f.write(writes)
		f.close()


	def add_images(self,folder):
		self.images_store.clear()
		for file_ in os.listdir(folder):
			name, type_ = os.path.splitext(file_)
			if type_ in [".png",".jpg"]:
				p_b = GdkPixbuf.Pixbuf.new_from_file_at_size(folder+"/"+file_,
															160,
															90)
				self.images_store.append([file_,p_b])



	def set_labels(self):
		self.apply_button.set_label("Apply")
		self.type_label.set_text("Wallpaper Type")


	def select_folder(self,widget):
		folder = self.file_choser.get_filename()
		self.add_images(folder)
