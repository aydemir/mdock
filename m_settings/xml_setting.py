import xml.etree.ElementTree as ET
import os



f = open(os.path.expanduser("~/.config/openbox/rc.xml"),"r")
root = ET.fromstring(f.read())
f.close()

ns = {'real_person': 'http://openbox.org/3.4/rc'}

def get_resistance():
	resistance = root.find('real_person:resistance', ns)
	strength = resistance.find('real_person:strength', ns)#strength Tells Openbox how much resistance (in pixels) there is between two windows before it lets them overlap.
	screen_edge_strength = resistance.find('real_person:screen_edge_strength', ns)#screen_edge_strength Basically the same as strength but between window and the screen edge.
	return {"strength":strength.text,"screen_edge_strength":screen_edge_strength.text}


def get_focus():
	focus = root.find('real_person:focus', ns)
	focusNew = focus.find('real_person:focusNew', ns)#Openbox will automatically give focus to new windows when they are created, otherwise the focus will stay as it is.
	focusLast = focus.find('real_person:focusLast', ns)#Makes focus follow mouse. e.g. when the mouse is being moved the focus will be given to window under the mouse cursor.
	followMouse = focus.find('real_person:followMouse', ns)#When switching desktops, focus the last focused window on that desktop again, regardless of where the mouse is. Only applies followMouse is set.
	focusDelay = focus.find('real_person:focusDelay', ns)#The time (in milliseconds) Openbox will wait before giving focus to the window under mouse cursor. Only applies if followMouse is set.
	underMouse = focus.find('real_person:underMouse', ns)#Focus windows under the mouse not only when the mouse moves, but also when it enters another window due to other reasons (e.g. the window the mouse was in moved/closed/iconified etc). Only applies if followMouse is set.
	raiseOnFocus = focus.find('real_person:raiseOnFocus', ns)#Also raises windows to top when they are focused. Only applies if followMouse is set.
	return {"focusNew":focusNew.text,"focusLast":focusLast.text,"followMouse":followMouse.text,
				"focusDelay":focusDelay.text,"underMouse":underMouse.text,"raiseOnFocus":raiseOnFocus.text}


def get_placement():
	placement = root.find('real_person:placement', ns)
	policy = placement.find('real_person:policy', ns)#policy can be either Smart or UnderMouse.
	#Smart will cause new windows to be placed automatically by Openbox.
	#UnderMouse makes new windows to be placed under the mouse cursor.
	center = placement.find('real_person:center', ns)#center can be either yes or no. If it is enabled, windows will open centered in the free area found.
	return {"policy":policy.text,"center":center.text}


def get_theme():
	theme = root.find('real_person:theme', ns)
	name = theme.find('real_person:name', ns)#name The name of the Openbox theme to use.
	titleLayout = theme.find('real_person:titleLayout', ns)#titleLayout tells in which order and what buttons should be in a window's titlebar. The following letters can be used, each only once.
	# N :window icon # L :window label (aka. title) # I: iconify
	# M: maximize # C: close # S: shade (roll up/down) # D: omnipresent (on all desktops).
	keepBorder = theme.find('real_person:keepBorder', ns)#keepBorder tells if windows should keep the border drawn by Openbox when window decorations are turned off.
	animateIconify = theme.find('real_person:animateIconify', ns)#animateIconify adds a little iconification animation if enabled. font Specifies the font to use for a specific element of the window. Place can be either of
	fonts = theme.findall('real_person:font', ns)
	find_fonts = []
	#This Places
	#ActiveWindow: Title bar of the active window
	#InactiveWindow: Title bar of the inactive window
	#MenuHeader: Titles in the menu
	#MenuItem: Items in the menu
	#OnScreenDisplay: Text in popups such as window cycling or desktop switching popups
	for font in fonts:
		f_place = font.attrib
		f_name = font.find('real_person:name', ns)
		f_place["name"] = f_name.text
		f_size = font.find('real_person:size', ns)
		f_place["size"] = f_size.text
		f_weight = font.find('real_person:weight', ns)
		f_place["weight"] = f_weight.text
		f_slant = font.find('real_person:slant', ns)
		f_place["slant"] = f_slant.text
		find_fonts.append(f_place)
	return {"name":name.text,"titleLayout":titleLayout.text,
			"keepBorder":keepBorder.text,"animateIconify":animateIconify.text,
			"fonts":find_fonts}


def get_desktop():
	desktop = root.find('real_person:desktops', ns)
	number = desktop.find('real_person:number', ns)#number The number of virtual desktops to use.
	firstdesk = desktop.find('real_person:firstdesk', ns)#firstdesk The number of the desktop to use when first started.
	popupTime = desktop.find('real_person:popupTime', ns)#popupTime Time (in milliseconds) to show the popup when switching desktops. Can be set to 0 to disable the popup completely.
	names = desktop.find('real_person:names', ns)#names Each name tag names your desktops, in ascending order. Unnamed desktops will be named automatically depending on the locale. You can name more desktops than specified in number if you want.
	find_names = []
	for name in names.findall('real_person:name', ns):
		find_names.append(name.text)
	return {"number":number.text,"firstdesk":firstdesk.text,"popupTime":popupTime.text,
			"names":find_names}


def get_resize():
	resize = root.find('real_person:resize', ns)
	drawContents = resize.find('real_person:drawContents', ns)#Resize the program inside the window while resizing. When disabled the unused space will be filled with a uniform color during a resize.
	popupShow = resize.find('real_person:popupShow', ns)#When to show the move/resize popup. Always always shows it, Never never shows it, Nonpixel shows it only when resizing windows that have specified they are resized in increments larger than one pixel, usually terminals.
	popupPosition = resize.find('real_person:popupPosition', ns)#Where to show the popup.
	#Top shows the popup above the titlebar of the window.
	#Center shows it centered on the window.
	#Fixed shows it in a fixed location on the screen specified by popupFixedPosition.
	popupFixedPosition = resize.find('real_person:popupFixedPosition', ns)#Specifies where on the screen to show the position when Fixed. Both x and y take coordinates as described here.
	pos_x = popupFixedPosition.find('real_person:x', ns)
	pos_y = popupFixedPosition.find('real_person:y', ns)
	return {"drawContents":drawContents.text,"popupShow":popupShow.text,
			"popupPosition":popupPosition.text,"pos_x":pos_x.text,"pos_y":pos_y.text}


def get_keyboard():
	keyboards = {}
	keyboard = root.find('real_person:keyboard', ns)
	for keybind in keyboard.findall('real_person:keybind', ns):
		key_name = keybind.attrib["key"]
		actions = keybind.findall('real_person:action', ns)
		for action in actions:
			action_t = get_action(action)
			keyboards[key_name] = action_t
	print(keyboards)


def get_action(action):
	action_name = action.attrib["name"]
	if action_name == "Execute":
		return {action_name:get_action_execute(action)}
		

def get_action_execute(action):
		command = action.find('real_person:command', ns)#A string which is the command to be executed, along with any arguments to be passed to it. The "~" tilde character will be expanded to your home directory, but no other shell expansions or scripting syntax may be used in the command unless they are passed to the sh command. Also, the & character must be written as &amp; in order to be parsed correctly. <execute> is a deprecated name for <command>.
		prompt = action.find('real_person:prompt', ns)#A string which Openbox will display in a popup dialog, along with "Yes" and "No" buttons. The execute action will only be run if you choose the "Yes" button in the dialog.
		notify = action.find('real_person:startupnotify', ns)
		if notify != None:
			enabled = notify.find('real_person:enabled', ns)#A boolean (yes/no) which says if the startup notification protocol should be used to notify other programs that an application is launching. This is disabled by default to avoid it being used for old-style xterminals.
			wmclass = notify.find('real_person:wmclass', ns)#A string specifying one of the values that will be in the application window's WM_CLASS property when the window appears. This is not needed for applications that support the startup-notification protocol.
			name = notify.find('real_person:name', ns)#The name of the application which is launching. If this option is not used, then the command itself will be used for the name.
			icon = notify.find('real_person:icon', ns)#The icon of the application which is launching. If this option is not used, then the command itself will be used to pick the icon.
			notify = {"enabled":is_none(enabled,"no"),"wmclass":is_none(wmclass,"none"),
					"name":is_none(name,"none"),"icon":is_none(icon,"none")}
		else:
			notify = {}
		return {"command":is_none(command,""),"prompt":is_none(prompt,"none"),
				"startupnotify":notify}


def is_none(control,default):
	if control == None:
		return default
	else:
		return control.text


#print(get_resistance())
#print(get_focus())
#print(get_placement())
#print(get_theme())
#print(get_desktop())
#print(get_resize())
print(get_keyboard())



"""
for child in root:
	print(child.tag, child.attrib)
	ch_tag = child.getroot()
	print(ch_tag)"""