#!/usr/bin/python3

from setuptools import setup, find_packages
import os


icons = []
for icon in os.listdir("./icons"):
	icons.append("icons/{}".format(icon))

datas = [("/usr/share/applications",["data/mdock.desktop","data/msettings.desktop"]),
		("/usr/share/fonts/OTF/", ["fonts/Digital Dismay.otf"]),
		("/usr/share/mtools/icons",icons),]


setup(
	name = "mtools",
	scripts = ["mdock","msettings"],
	packages = find_packages(),
	version = "0.1.5",
	description = "SoNAkıncı",
	author = ["Fatih Kaya"],
	author_email = "sonakinci41@gmail.com",
	url = "https://gitlab.com/sonakinci41/mdock",
	data_files = datas
)
