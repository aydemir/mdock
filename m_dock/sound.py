from gi.repository import Gtk, Gdk
import subprocess


class Sound(Gtk.Window):
	def __init__(self,sound, mute):
		Gtk.Window.__init__(self)
		self.connect("focus-out-event",self.focus_change)
		self.set_type_hint(Gdk.WindowTypeHint.UTILITY)
		self.set_decorated(False)

		self.menu_with = 75
		self.menu_height = 250
		m_box = Gtk.VBox()
		self.add(m_box)

		self.mixer = Gtk.Button()
		self.mixer.connect("clicked",self.open_mixer)
		self.mixer.set_label("Mixer")
		m_box.pack_start(self.mixer,False,True,5)


		ad1 = Gtk.Adjustment(value=sound, lower=0, upper=100, step_incr=5, page_incr=10, page_size=0)
		self.h_scale = Gtk.Scale(orientation=Gtk.Orientation.VERTICAL, adjustment=ad1)
		self.h_scale.connect("change_value",self.h_scale_change)
		self.h_scale.set_digits(0)
		self.h_scale.set_hexpand(True)
		m_box.pack_start(self.h_scale,True,True,5)
		self.set_size_request(self.menu_with, self.menu_height)

		self.mute_unmute = Gtk.Button()
		self.mute_unmute.connect("clicked",self.mute_unmute_change)
		if not mute:
			self.mute = "Mute"
		else:
			self.mute = "UnMute"
		self.mute_unmute.set_label(self.mute)
		m_box.pack_start(self.mute_unmute,False,True,5)

	def h_scale_change(self,widget,scroll,value):
		subprocess.Popen(["pactl","set-sink-volume","0","{}%".format(int(value))])


	def open_mixer(self,widget):
		subprocess.Popen(["pavucontrol"])
		self.destroy()


	def mute_unmute_change(self, widget):
		if self.mute == "Mute":
			self.mute = "UnMute"
		else:
			self.mute = "Mute"
		subprocess.Popen(["pactl","set-sink-mute","0","toggle"])
		self.mute_unmute.set_label(self.mute)


	def set_menu_position(self,pos_x,pos_y,dock_position,cursor,icon):
		if pos_x == 0:
			x = dock_position[2] + 20
			y = dock_position[1] + cursor[1] - self.menu_height / 2 + icon / 2
			self.h_scale.set_inverted(True)
		elif pos_x == 2:
			print(dock_position[0],self.menu_height)
			x = dock_position[0]-self.menu_with - 20
			y = dock_position[1] + cursor[1] - self.menu_height / 2 + icon / 2
			self.h_scale.set_inverted(True)

		if pos_y == 0:
			x = dock_position[0] + cursor[0] - self.menu_with / 2 + icon / 2
			y = dock_position[3] + 20
			self.h_scale.set_inverted(False)
		elif pos_y == 2:
			x = dock_position[0] + cursor[0] - self.menu_with / 2 + icon / 2
			y = dock_position[1] - self.menu_height - 20
			self.h_scale.set_inverted(True)#Yukardan aşağı aşağıdan yukarı

		self.move(x,y)
		self.set_default_size(self.menu_with,self.menu_height)
		self.show_all()

	def focus_change(self,window,event):
		self.destroy()



if __name__ == '__main__':
	pen = Sound(20,"mute")
	pen.show_all()
	Gtk.main()