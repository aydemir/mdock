import time, os, subprocess, psutil, math, iwlib
from m_dock import mmenu, sound
from gi.repository import Gtk, Gdk, GdkPixbuf, Wnck, GObject



class Dock(Gtk.Window):
	def __init__(self,screen):
		Gtk.Window.__init__(self)
		self.set_type_hint(Gdk.WindowTypeHint.DOCK)
		self.screen = screen

		self.settings()
		self.create_draw_area()
		self.time_loop = 0
		GObject.timeout_add(500, self.time_controls)


	def create_draw_area(self):
		self.draw_area = Gtk.DrawingArea()
		self.draw_area.connect("draw", self.draw_all)
		self.draw_area.set_events(Gdk.EventMask.BUTTON_PRESS_MASK | Gdk.EventMask.POINTER_MOTION_MASK | Gdk.EventMask.LEAVE_NOTIFY_MASK)
		self.draw_area.connect("button_press_event", self.click_window)
		self.draw_area.connect("motion-notify-event", self.cursor_move_window)
		self.draw_area.connect("leave-notify-event",self.cursor_leave_window)
		self.add(self.draw_area)


	def settings(self):
		self.screen_width = self.screen.get_width()
		self.screen_height = self.screen.get_height()
		self.draw_order = {"mmenu":lambda total_x, total_y ,cr: self.calc_mmenu(total_x, total_y ,cr),
						"apps":lambda total_x, total_y ,cr: self.calc_applications(total_x, total_y ,cr),
						"workspace":lambda total_x, total_y ,cr: self.calc_workspaces(total_x, total_y ,cr),
						"clock":lambda total_x, total_y ,cr: self.calc_clock(total_x, total_y ,cr),
						"battery":lambda total_x, total_y ,cr: self.calc_battery(total_x, total_y ,cr),
						"sound":lambda total_x, total_y ,cr: self.calc_sound(total_x, total_y ,cr),
						"wifi":lambda total_x, total_y ,cr: self.calc_wifi(total_x, total_y ,cr),}
		self.groups = []
		self.workspaces = []
		self.hidden_win_styles = [Wnck.WindowType.DESKTOP,Wnck.WindowType.DOCK]
		self.active_pos = [0,1,0,1]#x_1,y_1,x_2,y_2
		self.sound_info = ("","")
		self.dock_position = []
		self.popup_menu = False
		self.draw_mmenu = False
		self.draw_apps = False
		self.draw_clock = False
		self.draw_works = False
		self.draw_battery = False
		self.draw_sound = False
		self.draw_wifi = False
		self.mmenu = False
		self.sound_win = False
		self.wifi_info = False

		##################### CHANGEBLE SETTİNGS ########################
		self.dock_auto_hide = 1
		self.hor_ver = 0#0 horizontal 1 vertical
		self.position_x = 1# 0 is left 1 is center 2 is right
		self.position_y = 2# 0 is top 1 is center 2 is bottom
		self.size_x = 48
		self.size_y = 48
		self.padding_x = 5
		self.padding_y = 5
		self.size_focus_light = 2
		self.group_wins = 1
		self.show_desktop_dock = 0
		self.show_all_workspace = 1
		self.dock_move_workspace = 1
		self.clock_font_size = 36
		self.wifi_interface = "wlp2s0"
		self.draw_o = ["mmenu","apps","battery","sound","wifi","clock","workspace"]
		self.pinned_apps = []
		self.light_color_focus = (0.65,0.86,0.18,1)
		self.light_color_open = (0.46,0.44,0.36,1)
		self.clock_color = (0.81,0.81,0.76,1)
		self.hover_color = (1,1,1,0.5)
		##################################################################
		self.read_config_file()
		self.calc_icon_size()


	def read_config_file(self):
		dir_ = "~/.config/mdock/config"
		dir_ = os.path.expanduser(dir_)
		if os.path.exists(dir_):
			f = open(dir_,"r")
			read_ = f.read()
			f.close()
			read_ = read_.split("\n")
			for read in read_:
				read = read.split(":")
				if read[0] == "hor_ver":
					self.hor_ver = int(read[1])
				elif read[0] == "dock_auto_hide":
					self.dock_auto_hide = int(read[1])
				elif read[0] == "position_x":
					self.position_x = int(read[1])
				elif read[0] == "position_y":
					self.position_y = int(read[1])
				elif read[0] == "size_x":
					self.size_x = int(read[1])
				elif read[0] == "size_y":
					self.size_y = int(read[1])
				elif read[0] == "padding_x":
					self.padding_x = int(read[1])
				elif read[0] == "padding_y":
					self.padding_y = int(read[1])
				elif read[0] == "size_focus_light":
					self.size_focus_light = int(read[1])
				elif read[0] == "group_wins":
					self.group_wins = int(read[1])
				elif read[0] == "show_desktop_dock":
					self.show_desktop_dock = int(read[1])
				elif read[0] == "show_all_workspace":
					self.show_all_workspace = int(read[1])
				elif read[0] == "dock_move_workspace":
					self.dock_move_workspace = int(read[1])
				elif read[0] == "clock_font_size":
					self.clock_font_size = int(read[1])
				elif read[0] == "draw_o":
					if read[1] != "":
						a = read[1].replace(" ","")
						self.draw_o = a.split(",")
				elif read[0] == "pinned_apps":
					if read[1] != "":
						a = read[1].replace(" ","")
						self.pinned_apps = a.split(",")
				elif read[0] == "light_color_focus":
					a = read[1].split(",")
					self.light_color_focus = (float(a[0]),float(a[1]),float(a[2]),float(a[3]))
				elif read[0] == "light_color_open":
					a = read[1].split(",")
					self.light_color_open = (float(a[0]),float(a[1]),float(a[2]),float(a[3]))
				elif read[0] == "clock_color":
					a = read[1].split(",")
					self.clock_color = (float(a[0]),float(a[1]),float(a[2]),float(a[3]))
				elif read[0] == "hover_color":
					a = read[1].split(",")
					self.hover_color = (float(a[0]),float(a[1]),float(a[2]),float(a[3]))
		else:
			self.write_config_file()


	def write_config_file(self):
		config = "dock_auto_hide:{}\n".format(str(self.dock_auto_hide))
		config += "hor_ver:{}\n".format(str(self.hor_ver))
		config += "position_x:{}\n".format(str(self.position_x))
		config += "position_y:{}\n".format(str(self.position_y))
		config += "size_x:{}\n".format(str(self.size_x))
		config += "size_y:{}\n".format(str(self.size_y))
		config += "padding_x:{}\n".format(str(self.padding_x))
		config += "padding_y:{}\n".format(str(self.padding_y))
		config += "size_focus_light:{}\n".format(str(self.size_focus_light))
		config += "group_wins:{}\n".format(str(self.group_wins))
		config += "show_desktop_dock:{}\n".format(str(self.show_desktop_dock))
		config += "show_all_workspace:{}\n".format(str(self.show_all_workspace))
		config += "dock_move_workspace:{}\n".format(str(self.dock_move_workspace))
		config += "clock_font_size:{}\n".format(str(self.clock_font_size))
		config += "draw_o:{}\n".format(str(self.draw_o)[1:-1].replace("'",""))
		config += "pinned_apps:{}\n".format(str(self.pinned_apps)[1:-1].replace("'",""))
		config += "light_color_focus:{}\n".format(str(self.light_color_focus)[1:-1])
		config += "light_color_open:{}\n".format(str(self.light_color_open)[1:-1])
		config += "clock_color:{}\n".format(str(self.clock_color)[1:-1])
		config += "hover_color:{}".format(str(self.hover_color)[1:-1])


		file_ = "~/.config/mdock/config"
		file_ = os.path.expanduser(file_)
		dir_ = "~/.config/mdock"
		dir_ = os.path.expanduser(dir_)

		if not os.path.exists(dir_):
			os.mkdir(dir_)

		f = open(file_,"w")
		f.write(config)
		f.close()



################## SCRENN EVENTS ######################
	def remove_application(self, app):
		self.applications.remove(app)


	def add_application(self, app):
		self.applications.append(app)


	def add_class_group(self,group):
		if group.get_name() == "mdock":
			self.dock_window = group.get_windows()[0]
		self.groups.append(group)
		self.update_draw()


	def remove_class_group(self,group):
		self.groups.remove(group)
		self.update_draw()


	def add_workspace(self,workspace):
		self.workspaces.append(workspace)
		self.update_draw()


	def remove_workspace(self,workspace):
		self.workspaces.remove(workspace)
		self.update_draw()


	def update_draw(self):
		self.draw_area.queue_draw()
######################################################

##################### DRAWS ##########################
	def calc_icon_size(self):
		if self.hor_ver:
			a = self.size_x
			b = self.padding_x*2
		else:
			a = self.size_y
			b = self.padding_y*2
		self.icon_size = (a - b) - self.size_focus_light
		Wnck.set_default_icon_size(self.icon_size)


	def calc_icon_pos(self, start_x, start_y):
		if self.hor_ver:
			y_1 = start_y
			y_3 = start_y + self.padding_y
			if self.position_x == 0:
				x_1 = self.size_focus_light
				x_3 = 0
			else:
				x_1 = 0
				x_3 = self.icon_size + self.padding_x
		else:
			x_1 = start_x
			x_3 = start_x + self.padding_x
			if self.position_y == 0:
				y_1 = self.size_focus_light
				y_3 = 0
			else:
				y_1 = 0
				y_3 = self.icon_size + self.padding_y
		x_1 += self.padding_x
		y_1 += self.padding_y
		x_2 = x_1 + self.icon_size
		y_2 = y_1 + self.icon_size
		if self.position_y == 2:
			y_2 += self.size_focus_light
		if self.position_x == 2:
			x_2 += self.size_focus_light
		return (x_1,y_1,x_2,y_2,x_3,y_3)


	def draw_hover(self, cr):
		x,y,z,t = self.hover_color
		cr.set_source_rgba(x, y, z, t)
		cr.rectangle(self.active_pos[0], self.active_pos[1],
					self.active_pos[2]-self.active_pos[0],
					self.active_pos[3]-self.active_pos[1])
		cr.fill()


	def draw_applications(self,cr):
		for draw in self.draw_apps.keys():
			pos = self.draw_apps[draw]["pos"]
			if len(self.draw_apps[draw]["wins"]) == 0:
				path = os.path.expanduser("~/.config/mdock/pins/{}".format(draw))
				icon = GdkPixbuf.Pixbuf.new_from_file_at_size(path,self.icon_size,self.icon_size)
			else:
				icon = self.draw_apps[draw]["group"].get_icon()

				#draw light
				if self.win_is_active(self.draw_apps[draw]["group"]):
					x,y,z,t = self.light_color_focus
				else:
					x,y,z,t = self.light_color_open
				cr.set_source_rgba(x, y, z, t)
				if self.hor_ver:
					cr.rectangle(pos[4], pos[5], self.size_focus_light, self.icon_size)
				else:
					cr.rectangle(pos[4], pos[5], self.icon_size, self.size_focus_light)
				cr.fill()
			#icon = icon.scale_simple(self.icon_size,self.icon_size,GdkPixbuf.InterpType.NEAREST)
			#Draw Hover
			if pos == self.active_pos:
				self.draw_hover(cr)
			Gdk.cairo_set_source_pixbuf(cr,icon,pos[0],pos[1])
			cr.paint()


	def win_is_active(self,win):
		if type(win) == Wnck.ClassGroup:
			wins = win.get_windows()
			for win in wins:
				if win.is_active():
					return True
			return False
		else:
			return win.is_active()


	def calc_applications(self, total_x, total_y ,cr):
		total_x = total_x
		total_y = total_y
		self.draw_apps = {}

		if self.group_wins:
			groups = list(set(self.groups))
			for pin in self.pinned_apps:
				c = True
				for group in groups:
					if pin == group.get_id():
						g = group
						c = False
						break
				if c:
					x_1,x_2,total_x,total_y,z,t = self.calc_icon_pos(total_x,total_y)
					self.draw_apps[pin] = {"wins":[],
											"pos":(x_1,x_2,total_x,total_y,z,t)}
				else:
					wins = self.application_is_hidden(g.get_windows())
					if wins:
						x_1,x_2,total_x,total_y,z,t = self.calc_icon_pos(total_x,total_y)
						self.draw_apps[g.get_id()] = {"wins":wins,
											"pos":(x_1,x_2,total_x,total_y,z,t),
											"group":g}
						groups.remove(g)

			for group in groups:
				if group.get_name() == "mdock":
					continue
				wins = self.application_is_hidden(group.get_windows())
				if wins:
					x_1,x_2,total_x,total_y,z,t = self.calc_icon_pos(total_x,total_y)
					self.draw_apps[group.get_id()] = {"wins":wins,
										"pos":(x_1,x_2,total_x,total_y,z,t),
										"group":group}
		else:
			for group in self.groups:
				if group.get_name() == "mdock":
					continue
				wins = self.application_is_hidden(group.get_windows())
				if wins:
					for win in wins:
						x_1,x_2,total_x,total_y,z,t = self.calc_icon_pos(total_x,total_y)
						self.draw_apps[group.get_id()] = {"wins":[win],
											"pos":(x_1,x_2,total_x,total_y,z,t),
											"group":group}
		self.draw_applications(cr)
		return (total_x, total_y)


	def application_is_hidden(self,windows):
		"""Application visible or hidden"""
		wins = []
		workspace = self.screen.get_active_workspace()
		if self.show_desktop_dock:
			if self.show_all_workspace:
				wins = windows
			else:
				for win in windows:
					if win.get_workspace() == workspace:
						wins.append(win)
		else:
			for win in windows:
				if win.get_window_type() not in self.hidden_win_styles:
					if self.show_all_workspace:
						wins.append(win)
					else:
						if win.get_workspace() == workspace:
							wins.append(win)
		if len(wins) == 0:
			return False
		else:
			return wins


	def draw_workspaces(self, cr):
		for draw in self.draw_works.keys():
			pos = self.draw_works[draw]["pos"]
			if draw == self.screen.get_active_workspace():
				x,y,z,t = self.light_color_focus
			else:
				x,y,z,t = self.light_color_open
			cr.set_source_rgba(x, y, z, t)
			cr.rectangle(pos[0], pos[1], pos[2], pos[3])
			cr.fill()


	def calc_workspaces(self,total_x, total_y ,cr):
		t_x = total_x
		t_y = total_y
		if self.hor_ver:
			total_x = 0
		else:
			total_y = 0
		a =  len(self.workspaces)
		if a > 1:
			self.draw_works = {}
			box_size = (self.icon_size - self.padding_x) / 2 + self.size_focus_light / 2
			for x in range(0,a):
				x_1 = total_x + self.padding_x
				y_1 = total_y + self.padding_y
				if x % 2 == 1:#Kalan birse ikinci çiziliyor
					if self.hor_ver:
						total_y = y_1 + box_size
						x_1 += box_size + self.padding_x
					else:
						total_x = x_1 + box_size
						y_1 += box_size + self.padding_y
				else:
					if x == a-1:
						if self.hor_ver:
							total_y = y_1 + box_size
						else:
							total_x = x_1 + box_size		
				w_s = self.workspaces[x]
				self.draw_works[w_s] = {"workspace":w_s,
									"pos":(x_1,y_1,box_size,box_size)}
			self.draw_workspaces(cr)

			if self.hor_ver:
				total_x = t_x
			else:
				total_y = t_y
			return (total_x, total_y)
		else:
			return (t_x, t_y)


	def calc_clock(self,total_x, total_y ,cr):
		active_time = time.strftime('%X')
		(hr, mn, sc) = active_time.split(":")
		self.active_time = [hr,mn]
		x,y,z,t = self.clock_color
		cr.set_source_rgba(x, y, z, t)
		cr.select_font_face("Digital Dismay")
		cr.set_font_size(self.clock_font_size)
		if self.hor_ver:
			(x, y, width, height, dx, dy) = cr.text_extents(hr)
			total_x = self.padding_x
			total_y += self.padding_y+height
			self.draw_clock = [total_x,total_y-height]
			cr.move_to((self.icon_size+self.size_focus_light-width-x)/2+self.padding_x,total_y)
			cr.show_text(hr)
			cr.fill()
			total_y += self.padding_y+height
			cr.move_to((self.icon_size+self.size_focus_light-width-x)/2+self.padding_x,total_y)
			(x, y, width, height, dx, dy) = cr.text_extents(hr)
			cr.show_text(mn)
			cr.fill()
			self.draw_clock.append(total_x+self.icon_size)
			self.draw_clock.append(total_y)
		else:
			total_y = self.padding_y
			total_x += self.padding_x
			self.draw_clock = [total_x,total_y]
			(x, y, width, height, dx, dy) = cr.text_extents("{}:{}".format(hr,mn))
			cr.move_to(total_x,(self.icon_size+self.size_focus_light+height)/2+self.padding_y)
			cr.show_text("{}:{}".format(hr,mn))
			cr.fill()
			total_x += width + x
			self.draw_clock.append(total_x)
			self.draw_clock.append(total_y+self.icon_size)
		return (total_x, total_y)


	def calc_mmenu(self,total_x, total_y ,cr):
		if self.hor_ver:
			total_x = self.padding_x
			total_y = total_y + self.padding_y
		else:
			total_x = total_x + self.padding_x
			total_y = self.padding_y
		self.draw_mmenu = [total_x,total_y]

		
		if self.active_pos == [total_x,total_y,total_x+self.icon_size, total_y+self.icon_size]:
			x,y,z,t = self.light_color_open
			cr.set_source_rgba(x, y, z, t)
			cr.rectangle(total_x, total_y, self.icon_size, self.icon_size)
			cr.fill()

		icon_path = self.get_icon_dir("mmenu.svg")
		mmenu_pb = GdkPixbuf.Pixbuf.new_from_file_at_size(icon_path,self.icon_size,self.icon_size)
		Gdk.cairo_set_source_pixbuf(cr,mmenu_pb,total_x,total_y)
		cr.paint()

		total_x += self.icon_size
		total_y += self.icon_size
		self.draw_mmenu.append(total_x)
		self.draw_mmenu.append(total_y)
		return (total_x, total_y)


	def calc_battery(self,total_x, total_y ,cr):
		info_ = psutil.sensors_battery()
		if info_ == None:
			return (total_x,total_y)
		if self.hor_ver:
			total_x = self.padding_x
			total_y = total_y + self.padding_y
		else:
			total_x = total_x + self.padding_x
			total_y = self.padding_y
		self.draw_battery = [total_x,total_y]
		self.battery_percent = info_.percent
		self.battery_power_plugged = info_.power_plugged
		self.battery_time = info_.secsleft
		mm, ss = divmod(self.battery_time, 60)
		hh, mm = divmod(mm, 60)
		self.battery_time = "{}:{}:{}".format(hh, mm, ss)


		size = (self.icon_size + self.size_focus_light) /2
		m = self.icon_size / 10

		if self.battery_power_plugged:
			icon_name = "battery_up.svg"
		else:
			icon_name = "battery_down.svg"

		icon_path = self.get_icon_dir(icon_name)

		mmenu_pb = GdkPixbuf.Pixbuf.new_from_file_at_size(icon_path,size*2-m*4,size*2-m*4)
		Gdk.cairo_set_source_pixbuf(cr,mmenu_pb,total_x + m*2,total_y + m*2)
		cr.paint()

		x,y,z,t = self.light_color_open
		cr.set_source_rgba(x, y, z, t)
		cr.set_line_width(m*2)
		cr.arc(total_x + size, total_y + size, size - m, 0, math.pi*2)
		cr.stroke()

		x,y,z,t = self.light_color_focus
		cr.set_source_rgba(x, y, z, t)
		cr.set_line_width(m*2)
		cr.arc(total_x + size, total_y + size, size - m, 0, math.pi*2*self.battery_percent/100)
		cr.stroke()

		total_x += self.icon_size + self.size_focus_light
		total_y += self.icon_size + self.size_focus_light
		self.draw_battery.append(total_x)
		self.draw_battery.append(total_y)
		return (total_x, total_y)


	def calc_sound(self,total_x, total_y ,cr):
		if self.sound_info == ("",""):
			self.draw_sound = True
			return (total_x, total_y)

		if self.hor_ver:
			total_x = self.padding_x
			total_y = total_y + self.padding_y
		else:
			total_x = total_x + self.padding_x
			total_y = self.padding_y
		self.draw_sound = [total_x,total_y]
		self.volmu_percent = int(self.sound_info[0])
		self.volume_state = self.sound_info[1]


		size = (self.icon_size + self.size_focus_light) /2
		m = self.icon_size / 10

		if not self.volume_state:
			icon_name = "volume_up.svg"
		else:
			icon_name = "volume_down.svg"

		icon_path = self.get_icon_dir(icon_name)

		mmenu_pb = GdkPixbuf.Pixbuf.new_from_file_at_size(icon_path,size*2-m*4,size*2-m*4)
		Gdk.cairo_set_source_pixbuf(cr,mmenu_pb,total_x + m*2,total_y + m*2)
		cr.paint()

		x,y,z,t = self.light_color_open
		cr.set_source_rgba(x, y, z, t)
		cr.set_line_width(m*2)
		cr.arc(total_x + size, total_y + size, size - m, 0, math.pi*2)
		cr.stroke()

		x,y,z,t = self.light_color_focus
		cr.set_source_rgba(x, y, z, t)
		cr.set_line_width(m*2)
		cr.arc(total_x + size, total_y + size, size - m, 0, math.pi*2*self.volmu_percent/100)
		cr.stroke()

		total_x += self.icon_size + self.size_focus_light
		total_y += self.icon_size + self.size_focus_light
		self.draw_sound.append(total_x)
		self.draw_sound.append(total_y)
		return (total_x, total_y)


	def calc_wifi(self,total_x, total_y ,cr):
		size = (self.icon_size + self.size_focus_light) /2
		m = self.icon_size / 10

		self.draw_wifi = [total_x,total_y]
		if self.hor_ver:
			total_x = self.padding_x
			total_y = total_y + self.padding_y
		else:
			total_x = total_x + self.padding_x
			total_y = self.padding_y


		if self.wifi_info:
			icon_name = "wifi_up.svg"
		else:
			icon_name = "wifi_down.svg"

		icon_path = self.get_icon_dir(icon_name)

		mmenu_pb = GdkPixbuf.Pixbuf.new_from_file_at_size(icon_path,size*2-m*4,size*2-m*4)
		Gdk.cairo_set_source_pixbuf(cr,mmenu_pb,total_x + m*2,total_y + m*2)
		cr.paint()

		x,y,z,t = self.light_color_open
		cr.set_source_rgba(x, y, z, t)
		cr.set_line_width(m*2)
		cr.arc(total_x + size, total_y + size, size - m, 0, math.pi*2)
		cr.stroke()

		if self.wifi_info and self.wifi_info[1]:
			x,y,z,t = self.light_color_focus
			cr.set_source_rgba(x, y, z, t)
			cr.set_line_width(m*2)
			cr.arc(total_x + size, total_y + size, size - m, 0, math.pi*2*self.wifi_info[1]/70)
			cr.stroke()

		total_x += self.icon_size + self.size_focus_light
		total_y += self.icon_size + self.size_focus_light
		self.draw_wifi.append(total_x)
		self.draw_wifi.append(total_y)
		return (total_x, total_y)




		return (total_x, total_y)

	def draw_all(self, widget, cr):
		total_x = 0
		total_y = 0
		for draw in self.draw_o:
			func = self.draw_order[draw]
			total_x,total_y = func(total_x, total_y, cr)
		self.set_window_size(total_x,total_y)
########################################################


#####################GET INFO###########################
	def get_sound_info(self):
		try:
			out_ = subprocess.check_output(["/usr/bin/bash", "-c","LC_ALL=C pactl list sinks"])
			out_ = out_.decode("utf-8")
			out_ = out_.split("\n")
			for o in out_:
				o = o.replace(" ","")
				o = o.replace("\t","")
				a = o.split(":")
				if a[0] == "Volume":
					b = a[2]
					b = b.split("/")[1]
					b = b.split("%")[0]
					percent = int(b)
				if a[0] == "Mute":
					if a[1] == "yes":
						is_mute = True
					else:
						is_mute = False				
			return (percent, is_mute)
		except:
			return False

	def get_wifi_info(self):
		subprocess.Popen(["nmcli","d","wifi","rescan"])
		out_ = subprocess.check_output(["nmcli","d","wifi","list"])
		out_ = out_.decode("utf-8")
		out_ = out_.split("\n")
		connect_wifi = False
		wifis = []
		for out in out_:
			wifi = out.split()
			if len(wifi) == 0:
				continue
			elif wifi[0] == "IN-USE":
				continue
			elif wifi[0] == "*":
				connect_wifi = {"name" : wifi[2], "power" : wifi[7]}
			else:
				wifis.append({"name" : wifi[1], "power" : wifi[6]})
		return (connect_wifi,wifis)
########################################################





################# POSİTİON SIZE #######################
	def set_window_position(self,total_x,total_y):
		if self.position_y == 0:
			y = 0
		elif self.position_y == 1:
			y = (self.screen_height - total_y) / 2
		elif self.position_y == 2:
			y = self.screen_height - total_y
		if self.position_x == 0:
			x = 0
		elif self.position_x == 1:
			x = (self.screen_width - total_x) / 2
		elif self.position_x == 2:
			x = self.screen_width - total_x
		self.dock_position = (x, y, x+total_x, y+total_y)
		self.move(x, y)



	def set_window_size(self,total_x,total_y):
		total_x = total_x
		total_y = total_y
		if self.hor_ver:
			total_x = self.size_x
			total_y += self.padding_y

		else:
			total_x += self.padding_x
			total_y = self.size_y
		self.set_size_request(total_x,total_y)
		self.set_resizable(False)
		self.set_window_position(total_x,total_y)
########################################################


################### AREA EVENTS #########################
	def click_window(self,widget,pos):
		#LEFT CLICK
		if pos.button == 1:
			if self.draw_apps:
				for draw in self.draw_apps.keys():
					d = self.draw_apps[draw]
					pos_ = d["pos"]
					wins = d["wins"]
					if self.active_pos == pos_:
						if len(wins) == 0:
							subprocess.Popen(draw.lower().split())
						elif len(wins) == 1:
							win = wins[0]
							self.win_state_change(win)
						else:
							self.popup_menu = Gtk.Menu()
							for win in wins:
								image = Gtk.Image()
								image.set_from_pixbuf(win.get_icon())
								item = Gtk.ImageMenuItem(win.get_name())
								item.set_image(image)
								item.connect("activate",self.menu_clicked,win)
								self.popup_menu.append(item)
							self.popup_menu.show_all()
							self.popup_menu.popup_at_pointer()
							return True
			if self.draw_works:
				for draw in self.draw_works.keys():
					d = self.draw_works[draw]
					pos_ = d["pos"]
					if self.active_pos == pos_:
						d["workspace"].activate(0)
						if self.dock_move_workspace:
							self.dock_window.move_to_workspace(d["workspace"])
							return True
			if self.draw_clock and self.active_pos == self.draw_clock:
				return True
			elif self.draw_mmenu and self.draw_mmenu[0] < pos.x < self.draw_mmenu[2] and self.draw_mmenu[1] < pos.y < self.draw_mmenu[3]:
				if not self.mmenu and "mmenu" in self.draw_o:
					self.mmenu = mmenu.MMenu()
					self.mmenu.set_menu_position(self.position_x,self.position_y,self.dock_position,self.draw_mmenu)
				else:
					if self.mmenu.is_visible():
						self.mmenu.hide()
					else:
						self.mmenu.set_menu_position(self.position_x,self.position_y,self.dock_position,self.draw_mmenu)
			elif self.draw_sound and self.draw_sound[0] < pos.x < self.draw_sound[2] and self.draw_sound[1] < pos.y < self.draw_sound[3]:
				if self.sound_win and self.sound_win.is_visible():
					self.sound_win.destroy()
				else:
					self.sound_win = sound.Sound(self.volmu_percent,self.volume_state)
					self.sound_win.set_menu_position(self.position_x,self.position_y,self.dock_position,self.draw_sound,self.icon_size)
			elif self.draw_battery and self.draw_battery[0] < pos.x < self.draw_battery[2] and self.draw_battery[1] < pos.y < self.draw_battery[3]:
				self.popup_menu = Gtk.Menu()
				item = Gtk.MenuItem("Time : {}\nPercent : % {}\nPower Plugget : {}".format(self.battery_time,int(self.battery_percent),self.battery_power_plugged))
				self.popup_menu.append(item)
				self.popup_menu.show_all()
				self.popup_menu.popup_at_pointer()
				


		#RIGHT CLICK
		if pos.button == 3:
			self.popup_menu = Gtk.Menu()
			if self.draw_apps:
				for draw in self.draw_apps.keys():
					d = self.draw_apps[draw]
					pos_ = d["pos"]
					wins = d["wins"]
					if self.active_pos == pos_:
						if len(wins) == 1:
							if wins[0].is_active():
								image = Gtk.Image()
								image.set_from_stock(Gtk.STOCK_GO_DOWN,Gtk.IconSize.MENU)
								item = Gtk.ImageMenuItem("Minimize")
								item.set_image(image)
								item.connect("activate",self.win_popup_clicked,wins[0],"minimize")
								self.popup_menu.append(item)
							else:
								image = Gtk.Image()
								image.set_from_stock(Gtk.STOCK_GO_UP,Gtk.IconSize.MENU)
								item = Gtk.ImageMenuItem("Active")
								item.set_image(image)
								item.connect("activate",self.win_popup_clicked,wins[0],"activate")
								self.popup_menu.append(item)
							image = Gtk.Image()
							image.set_from_stock(Gtk.STOCK_CLOSE,Gtk.IconSize.MENU)
							item = Gtk.ImageMenuItem("Close Window")
							item.set_image(image)
							item.connect("activate",self.win_popup_clicked,wins[0],"close")
							self.popup_menu.append(item)
						if len(wins) != 0 and d["group"].get_id() not in self.pinned_apps:
							image = Gtk.Image()
							image.set_from_stock(Gtk.STOCK_ADD,Gtk.IconSize.MENU)
							item = Gtk.ImageMenuItem("Pinned Dock")
							item.set_image(image)
							item.connect("activate",self.pinned_win,d)
							self.popup_menu.append(item)
						else:
							image = Gtk.Image()
							image.set_from_stock(Gtk.STOCK_DELETE,Gtk.IconSize.MENU)
							item = Gtk.ImageMenuItem("Un Pinned Dock")
							item.set_image(image)
							item.connect("activate",self.un_pinned_win,draw)
							self.popup_menu.append(item)
						break
			image = Gtk.Image()
			image.set_from_stock(Gtk.STOCK_PREFERENCES,Gtk.IconSize.MENU)
			item = Gtk.ImageMenuItem("Settings")
			item.set_image(image)
			item.connect("activate",self.dock_settings_click)
			self.popup_menu.append(item)
			self.popup_menu.show_all()
			self.popup_menu.popup_at_pointer()


	def get_icon_dir(self,icon):
		icon_path = "./icons/{}".format(icon)
		if not os.path.exists(icon_path):
			icon_path = os.path.expanduser("~/.config/mdock/icons/{}".format(icon))
			if not os.path.exists(icon_path):
				icon_path = "/usr/share/mtools/icons/{}".format(icon)
		return icon_path



	def dock_settings_click(self, widget):
		subprocess.Popen("msettings")


	def pinned_win(self,widget,app):
		id_ = app["group"].get_id()
		icon = app["group"].get_icon()
		path = os.path.expanduser("~/.config/mdock/pins/{}".format(id_))
		path_ = os.path.expanduser("~/.config/mdock/pins")
		if not os.path.exists(path_):
			os.mkdir(path_)
		try:
			icon.savev(path,"png",["quality"], ["100"])
		except:
			print("DONT SAVE İCON")
		self.pinned_apps.append(id_)
		self.write_config_file()


	def un_pinned_win(self,widget,id_):
		path = os.path.expanduser("~/.config/mdock/pins/{}".format(id_))
		os.remove(path)
		self.pinned_apps.remove(id_)
		self.write_config_file()


	def menu_clicked(self,widget,win):
		self.win_state_change(win)


	def win_popup_clicked(self,widget,win,action):
		if action == "close":
			win.close(0)
		if action == "minimize":
			win.minimize()
		if action == "activate":
			win.activate(0)


	def win_state_change(self, win):
		if win.is_active():
			win.minimize()
		else:
			win.activate(0)
		if self.show_all_workspace:
			if win.get_workspace() != self.screen.get_active_workspace():
				win.move_to_workspace(self.screen.get_active_workspace())


	def cursor_move_window(self,widget,pos):
		if self.draw_apps:
			for draw in self.draw_apps.keys():
				d = self.draw_apps[draw]
				pos_ = d["pos"]
				if pos_[0] < pos.x < pos_[2] and pos_[1] < pos.y < pos_[3]:
					if self.active_pos != pos_:
						self.active_pos = pos_
						self.update_draw()
						return True
		if self.draw_works:
			for draw in self.draw_works.keys():
				d = self.draw_works[draw]
				pos_ = d["pos"]
				if pos_[0] < pos.x < pos_[0]+pos_[2] and pos_[1] < pos.y < pos_[1]+pos_[3]:
					if self.active_pos != pos_:
						self.active_pos = pos_
						self.update_draw()
						return True
		if self.draw_clock and self.draw_clock[0] < pos.x < self.draw_clock[2] and self.draw_clock[1] < pos.y < self.draw_clock[3]:
			if self.active_pos != self.draw_clock:
				self.update_draw()
				self.active_pos = self.draw_clock
				return True
		if self.draw_mmenu and self.draw_mmenu[0] < pos.x < self.draw_mmenu[2] and self.draw_mmenu[1] < pos.y < self.draw_mmenu[3]:
			if self.active_pos != self.draw_mmenu:
				self.update_draw()
				self.active_pos = self.draw_mmenu
				return True
		if self.draw_sound and self.draw_sound[0] < pos.x < self.draw_sound[2] and self.draw_sound[1] < pos.y < self.draw_sound[3]:
			if self.active_pos != self.draw_sound:
				self.update_draw()
				self.active_pos = self.draw_sound
				return True
		if self.draw_battery and self.draw_battery[0] < pos.x < self.draw_battery[2] and self.draw_battery[1] < pos.y < self.draw_battery[3]:
			if self.active_pos != self.draw_battery:
				self.update_draw()
				self.active_pos = self.draw_battery
				return True


	def cursor_leave_window(self,widget,pos):
		self.active_pos = [0,1,0,1]
		self.update_draw()

########################################################

	def time_controls(self):
		redraw = False
		if self.draw_sound:
			sound = self.get_sound_info()
			if sound and self.sound_info != sound:
				self.sound_info = sound
				redraw = True
		if self.active_time != time.strftime('%X').split(":")[:-1]:
			redraw = True
		if self.dock_auto_hide:
			self.dock_visible_control()
		if self.draw_wifi:
			wifi = iwlib.get_iwconfig(self.wifi_interface)
			wifi_name = wifi['ESSID'].decode()
			wifi_quality = wifi.get('stats',False)
			if wifi_quality:
				wifi_quality = wifi_quality.get('quality',False)
			wifi_info = (wifi_name,wifi_quality)
			if self.wifi_info != wifi_info:
				self.wifi_info = wifi_info
				if wifi_name != "":
					redraw = True
		if redraw:
			self.update_draw()
		return True



##################### DOCK VİSİBLE #####################
	def dock_visible_control(self):
		win = self.screen.get_active_window()
		if win == None:
			if not self.is_visible():
				self.show()
			return True
		geo = win.get_geometry()
		win_pos = [geo[0], geo[1], geo[0] + geo[2], geo[1] + geo[3]]
		win_on = self.win_on_dock(win_pos)
		cur_on = self.cursor_on_dock()
		if win_on:#Hide
			if cur_on:
				if not self.is_visible():
					self.show()
			else:
				if self.is_visible():
					if self.popup_menu == False:
						self.hide()
					elif not self.popup_menu.is_visible():
						self.hide()
		else:#show
			if not self.is_visible():
				self.show()
		return True


	def cursor_on_dock(self):
		root_win = Gdk.get_default_root_window()
		pointer = root_win.get_pointer()
		if self.dock_position[0] < pointer.x < self.dock_position[2] and self.dock_position[1] < pointer.y < self.dock_position[3]:
			return True
		elif self.dock_position[0] < pointer.x < self.dock_position[2] and self.dock_position[1] < pointer.y < self.dock_position[3]:
			return True
		return False


	def win_on_dock(self,win_pos):
		if self.dock_position[0] < win_pos[0] < self.dock_position[2] and self.dock_position[1] < win_pos[1] < self.dock_position[3]:
			return True
		elif self.dock_position[0] < win_pos[0] < self.dock_position[2] and self.dock_position[1] < win_pos[3] < self.dock_position[3]:
			return True
		elif self.dock_position[0] < win_pos[2] < self.dock_position[2] and self.dock_position[1] < win_pos[1] < self.dock_position[3]:
			return True
		elif self.dock_position[0] < win_pos[2] < self.dock_position[2] and self.dock_position[1] < win_pos[3] < self.dock_position[3]:
			return True

		if win_pos[0] < self.dock_position[0] < win_pos[2] and win_pos[1] < self.dock_position[1] < win_pos[3]:
			return True
		elif win_pos[0] < self.dock_position[0] < win_pos[2] and win_pos[1] < self.dock_position[3] < win_pos[3]:
			return True
		elif win_pos[0] < self.dock_position[2] < win_pos[2] and win_pos[1] < self.dock_position[1] < win_pos[3]:
			return True
		elif win_pos[0] < self.dock_position[2] < win_pos[2] and win_pos[1] < self.dock_position[3] < win_pos[3]:
			return True
		return False
#######################################################

