import subprocess



def get_list_wifi():
	subprocess.Popen(["nmcli","d","wifi","rescan"])
	out_ = subprocess.check_output(["nmcli","d","wifi","list"])
	out_ = out_.decode("utf-8")
	out_ = out_.split("\n")
	connect_wifi = False
	wifis = []
	for out in out_:
		wifi = out.split()
		if len(wifi) == 0:
			continue
		elif wifi[0] == "IN-USE":
			continue
		elif wifi[0] == "*":
			connect_wifi = {"name" : wifi[2], "power" : wifi[7]}
		else:
			wifis.append({"name" : wifi[1], "power" : wifi[6]})
	return (connect_wifi,wifis)
			



print(get_list_wifi())