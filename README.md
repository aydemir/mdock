## Mdock

# For the app to work properly

python-gobject, python-psutil, python-iwlib

feh, pavucontrol, nmcli

# ScreenShots
![](https://gitlab.com/sonakinci41/mdock/-/raw/master/screenshots/1.png)
![](https://gitlab.com/sonakinci41/mdock/-/raw/master/screenshots/2.png)
![](https://gitlab.com/sonakinci41/mdock/-/raw/master/screenshots/3.png)
![](https://gitlab.com/sonakinci41/mdock/-/raw/master/screenshots/5.png)
![](https://gitlab.com/sonakinci41/mdock/-/raw/master/screenshots/6.png)
![](https://gitlab.com/sonakinci41/mdock/-/raw/master/screenshots/7.png)
